
function Nav() {
    return (
    <nav className="navbar navbar-expand-lg navbar-light bg-light">
    <div className="container-fluid">
      <a className="navbar-brand" href="#">Conference GO!</a>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <a className="nav-link text-dark" aria-current="page" href="http://localhost:3000/">Home</a>
            <a className="nav-link text-muted" aria-current="page" href="http://localhost:3000/new-location.html" id="navlocation">New location</a>
            <a className="nav-link text-muted" aria-current="page" href="http://localhost:3000/new-conference.html" id="navconference">New conference</a>
        </div>
    </div>
    </nav>
    );
  }

  export default Nav;
